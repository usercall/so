package planificacion.controlador;

import java.awt.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collections;
import planificacion.modelo.Cola;
import planificacion.modelo.Data;
import planificacion.vista.Ventana;

public class Gestor {

    private Ventana ventana;
    private Data data;

    private boolean isRunnable;
    int MIN_TURNO = 1;
    int MAX_TURNO = 10;

    public Gestor() {
        isRunnable = false;
        this.data = new Data();
        this.ventana = new Ventana();

        ventana.addBtnAgregar(new ListenerBtnAtender());
        ventana.addBtnGenerar(new ListenerBtnGenerar());
        ventana.addBtnEjecutar(new ListenerBtnEjecutar());

    }

    public void agregarProceso(int id, String nombre, int tLlegada, int tRafaga) {

        String ms = data.getCola().insertar(id, nombre, tLlegada, tRafaga);

        //Ordenar 
        ventana.mostarMenssaje(ms);
        ventana.refrescar(data.buildDataAll(data.getCola(),data.colaAtedidos), false);
    }

    public void ejecutarProceso() {
        // FIXME : no se puede de usna sola
        data.atenderProceso();

        //ventana.refrescar(data.buildData(data.colaAtedidos), true);
     ventana.refrescar(data.buildDataAll(data.getCola(),data.colaAtedidos), true);
    }

    public int generarNumero(int inicio, int fin) {
        int valorEntero = (int) Math.floor(Math.random() * (fin - inicio + 1) + inicio);
        return valorEntero;
    }

    public Data getData() {
        return data;
    }

///////////////////////////////////////Listeners///////////////////////////////////
    class ListenerBtnAtender implements ActionListener {

        public void actionPerformed(ActionEvent a) {

            try {
                agregarProceso(Integer.parseInt(ventana.getTxtId().getText()), ventana.getTxtNombre().getText(), Integer.parseInt(ventana.getTxtTLlegada().getText()), Integer.parseInt(ventana.getTxtTRafaga().getText()));
            } catch (NumberFormatException e) {
                ventana.mostarMenssaje("Datos mal ingresados");
            }
        }
    }

    class ListenerBtnGenerar implements ActionListener {

        public void actionPerformed(ActionEvent a) {
            ventana.rellenarDatos(generarNumero(MIN_TURNO, MAX_TURNO), String.valueOf((char) generarNumero(65, 91)), generarNumero(MIN_TURNO, MAX_TURNO), generarNumero(MIN_TURNO, MAX_TURNO));
        }
    }

    /*ejecucion del programa  */
    class ListenerBtnEjecutar implements ActionListener {

        public void actionPerformed(ActionEvent a) {
            isRunnable = !isRunnable;
            ventana.changeRunableBtn(isRunnable);
            if (isRunnable) {
                ejecutarProceso();
            }
        }
    }
}
