package planificacion.modelo;

public class Cola {

    private Nodo ultimo;
    private Nodo primero;
    private int tamano;
    int idx;

    public Cola() {
        tamano = 0;
        primero = new Nodo();
        primero.setNombreProceso("cabeza");
        primero.setSig(ultimo);
        ultimo = primero;
        ultimo.setSig(primero);
        idx = 0;
    }

    /**
     * Implementacion del algortinmo *
     */
    public String insertar(int id, String nombre, int tLlegada, int tRafaga) {

        String msg = "Se ha insertado el proceso";

        // rompemos el proceso 
        if (ultimo.gettFinal() > tLlegada) {
            msg = " El proceso se ha demorado en llegar tiempo actual :  " + Integer.toString(ultimo.gettFinal()) + "  sera atendido de inmediato";
        }

        Nodo q = new Nodo();
        q.setIdProceso(idx);
        q.setNombreProceso(nombre);
        q.settLlegada(tLlegada);
        q.settRafaga(tRafaga);

        q.setSig(primero);

        primero.settFinal(q.gettFinal());

        ultimo.setSig(q);
        ultimo = q;

        idx++;
        tamano++;
        return msg;
    }

    public void atender(Nodo q) {
        // forzamos para que empiece el primero de una 
        if (ultimo.gettFinal() == 0) {
            q.settComienzo(q.gettLlegada());
        } else {
            q.settComienzo(ultimo.gettFinal());
        }

        //calculos basicos
        q.settFinal(q.gettRafaga() + q.gettComienzo());
        q.settRetorno(q.gettFinal() - q.gettLlegada());
        q.settEspera(q.gettRetorno() - q.gettRafaga());

        //Forzando no datos negativos 
        if (q.gettEspera() < 0) {
            int offset = -1 * q.gettEspera();
            q.settEspera(0);
            q.settComienzo(q.gettComienzo() + offset);
            q.settFinal(q.gettFinal() + offset);
            q.settRetorno(q.gettRetorno() + offset);
        }

        q.setSig(primero);
        primero.settFinal(q.gettFinal());
        ultimo.setSig(q);
        ultimo = q;

        tamano++;
    }

    public Nodo retirar() {
        Nodo aux = primero.getSig();
        if (aux != primero) {
            primero.setSig(aux.getSig());
            if (aux == ultimo) {
                ultimo = aux.getSig();
            }
            aux.setSig(null);
            tamano--;
        }
        return aux;

        /*if(aux==p){
            return aux;
        }
        if(aux!=cab){  
            p.setSig(aux.getSig());
            aux.setSig(null);
            return aux;
        }
        else{
            cab=aux.getSig();
        }*/
    }

    public boolean isVacia() {
        boolean val = false;
        if (primero == ultimo) {
            val = true;
        }
        return val;
    }

    public Nodo getPrimero() {
        return primero;
    }

    public int getTamano() {
        return tamano;
    }

}
