package planificacion.modelo;

public class Data {

    private Cola cola;
    public Cola colaAtedidos;

    public Data() {
        this.cola = new Cola();
        this.colaAtedidos = new Cola();
    }

    public void atenderProceso() {
        if (!cola.isVacia()) {
            colaAtedidos.atender(cola.retirar());
        }
    }

    private Object centinela99(int n) {
        return (n == -99) ? " " : n;
    }

    public Object[][] buildData(Cola c) {
        // datos de tabla 
        Object[][] datos;
        if (!c.isVacia()) {

            datos = new Object[c.getTamano()][8];
            Nodo nodo = c.getPrimero().getSig();

            // organizar 
            for (int i = 0; i < c.getTamano(); i++) {
                datos[i][0] = nodo.getIdProceso();
                datos[i][1] = nodo.getNombreProceso();
                datos[i][2] = nodo.gettLlegada();
                datos[i][3] = nodo.gettRafaga();
                datos[i][4] = centinela99(nodo.gettComienzo());
                datos[i][5] = centinela99(nodo.gettFinal());
                datos[i][6] = centinela99(nodo.gettRetorno());
                datos[i][7] = centinela99(nodo.gettEspera());
                nodo = nodo.getSig();
            }
        } else {
            System.out.println("Cola vacia");
            datos = new Object[1][8];
            for (int i = 0; i < 8; i++) {
                datos[0][i] = "";
            }
        }
        return datos;
    }

    /*
    public Object[][] buildDataAll(Cola[] c) {
        // datos de tabla 
        Object[][] datos;
        
            
        int tamanioAll=0;
        for (int y = 0; y < 2; y++) {
            tamanioAll=+c[y].getTamano();
        }
        
        System.out.println("planificacion.modelo.Data.buildDataAll()" + tamanioAll);
        datos = new Object[tamanioAll][8];
      

        for (int y = 0; y < c.length; y++) {
              Nodo nodo = c[y].getPrimero().getSig();
        
        // organizar 
        for (int i = 0; i < c[y].getTamano(); i++) {
            datos[i][0] = nodo.getIdProceso();
            datos[i][1] = nodo.getNombreProceso();
            datos[i][2] = nodo.gettLlegada();
            datos[i][3] = nodo.gettRafaga();
            datos[i][4] =  centinela99(nodo.gettComienzo());
            datos[i][5] = centinela99(nodo.gettFinal());
            datos[i][6] = centinela99(nodo.gettRetorno());
            datos[i][7] = centinela99(nodo.gettEspera());  
            nodo = nodo.getSig();
        }}
        return datos;
    } 
     */
    public Object[][] buildDataAll(Cola a, Cola b) {
        // datos de tabla 
        Object[][] datos;

        int tamanioAll = a.getTamano() + b.getTamano();

        System.out.println("planificacion.modelo.Data.buildDataAll() cantidad Tootal de datos:" + tamanioAll);
        datos = new Object[tamanioAll][8];

        Nodo nodo = a.getPrimero().getSig();

        // organizar 
        int i;
        for (i = 0; i < a.getTamano(); i++) {
            datos[i][0] = nodo.getIdProceso();
            datos[i][1] = nodo.getNombreProceso();
            datos[i][2] = nodo.gettLlegada();
            datos[i][3] = nodo.gettRafaga();
            datos[i][4] = centinela99(nodo.gettComienzo());
            datos[i][5] = centinela99(nodo.gettFinal());
            datos[i][6] = centinela99(nodo.gettRetorno());
            datos[i][7] = centinela99(nodo.gettEspera());
            nodo = nodo.getSig();
        }
        nodo = b.getPrimero().getSig();

        // organizar 
        for (i = i; i < b.getTamano() + a.getTamano(); i++) {
            datos[i][0] = nodo.getIdProceso();
            datos[i][1] = nodo.getNombreProceso();
            datos[i][2] = nodo.gettLlegada();
            datos[i][3] = nodo.gettRafaga();
            datos[i][4] = centinela99(nodo.gettComienzo());
            datos[i][5] = centinela99(nodo.gettFinal());
            datos[i][6] = centinela99(nodo.gettRetorno());
            datos[i][7] = centinela99(nodo.gettEspera());
            nodo = nodo.getSig();
        }
        return datos;
    }

    public Cola getCola() {
        return cola;
    }

}
